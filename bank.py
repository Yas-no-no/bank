import json
from datetime import date, timedelta, datetime

act_date = "2023-01-01"


def load_users():
    with open('clients.json', 'r', encoding="utf-8") as file:
        file_contents = file.read()
        return json.loads(file_contents)


def save_users(self):
    with open('clients.json', 'w', encoding="utf-8") as file:
        json.dump(self, file, ensure_ascii=False, default=str)


def load_operations():
    with open('operations.json', 'r', encoding="utf-8") as file:
        file_operations = file.read()
        return json.loads(file_operations)


def save_operations(self):
    with open('operations.json', 'w', encoding="utf-8") as file:
        json.dump(self, file, ensure_ascii=False, default=str)


def add_client(num):
    file_data = load_users()
    names = {
        'phone_num': num,
        'name': input(),
        'date_cr': date.today(),
        'act_date': date.today(),
        'debit_card': 0,
        'sav_account': 0
    }
    file_data.append(names)
    save_users(file_data)


def edit_client():
    clients = load_users()
    print(clients[0]['name'])
    for i in clients:
        if i['phone_num'] == int(input()):
            i['name'] = input()
    save_users(clients)


def delete_client():
    clients = load_users()
    for i in range(len(clients)):
        if clients[i]['phone_num'] == int(input()):
            del clients[i]
    save_users(clients)


def action_menu():
    while True:
        print('1 добавить, 2 изменить, 4 стоп')
        action = int(input())
        if action == 1:
            num = int(input('Введите номер телефона и имя клиента'))
            if user['phone_num'] == num:
                print('user already exist')
                continue
            add_client()
        elif action == 2:
            print('Введите номер телефона и новое имя клиента')
            edit_client()
        # elif action == 3:
        #     print('Удалить клиента по номеру телефона')
        #     delete_client()
        elif action == 4:
            break


# action_menu()


data = load_users()


def select_user():
    num = input('Введите номер телефона с 8 для операций с счетами: ')
    num = int(num)
    for i in data:
        if i['phone_num'] == num:
            print(i['name'], i['phone_num'], i['date_cr'], i['debit_card'])
    return num


numb = select_user()


days = int(input('days pass: '))
new_act_date = act_date.split('-')
new_date = date(int(new_act_date[0]), int(new_act_date[1]), int(new_act_date[2]))
bb = timedelta(days=days)
act_date = new_date + bb


def count_days(f_date, s_date):
    d1 = datetime.strptime(f_date, '%Y-%m-%d')
    d2 = datetime.strptime(s_date, '%Y-%m-%d')
    day = (d2 - d1).days
    return day


transaction = load_operations()


class BankAccount:
    def __init__(self, number):
        self.number = number

    def add_money(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                user['debit_card'] += amount
                operations = {
                    'phone_num': self.number,
                    'date': datetime.now(),
                    'account_to': 'debit_card',
                    'amount': amount,
                    'comment': input('comment transaction ')
                }
                transaction.append(operations)

    def delete_money(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                if amount <= user['debit_card']:
                    user['debit_card'] -= amount
                else:
                    print('больше чем остаток на карте')
        operations = {
            'phone_num': self.number,
            'date': datetime.now(),
            'account_from': 'debit_card',
            'amount': amount,
            'comment': input('comment transaction ')
        }
        transaction.append(operations)

    def days_pass(self):
        for user in data:
            if user['phone_num'] == self.number:
                if user['act_date'] != act_date:
                    count_days(act_date, user['act_date'])
                    if day // 30 >= 1:
                        if user['debit_card'] >= ((day // 30) * 100):
                            user['debit_card'] -= (day // 30) * 100
                            if user['debit_card'] < 0:
                                user['debit_card'] = 0
                    for i in day:
                        user['sav_account'] *= 0.05 * 1 / 365 
                        user['pen_account'] *= 0.03 * 1 / 365 
                        i += 1
                if user['deposit_90_date_cr'] != act_date
                    count_days(act_date, user['deposit_90_date_cr'])
                        if day >= 91 and user['deposit_90'] > 0
                            user['deposit_90'] = (user['deposit_90'] * 0,06) * 90 / 365
                            user['debit_card'] += user['deposit_90']
                            delete user['deposit_90']

                if user['deposit_180_date_cr'] != act_date
                    count_days(act_date, user['deposit_180_date_cr'])   
                        if day >= 181 and user['deposit_180'] > 0
                            user['deposit_180'] = (user['deposit_180'] * 0,06) * 180 / 365
                            user['debit_card'] += user['deposit_180']
                            delete user['deposit_180']
 
                if user['deposit_360_date_cr'] != act_date
                    count_days(act_date, user['deposit_360_date_cr'])    
                        if day >= 361 and user['deposit_360'] > 0
                            user['deposit_360'] = (user['deposit_360'] * 0,06) * 360 / 365
                            user['debit_card'] += user['deposit_360']
                            delete user['deposit_360']
                



class SavingsAccount(BankAccount):
    def add_money(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                if amount <= user['debit_card']:
                    user['debit_card'] -= amount
                    user['sav_account'] += amount
                else:
                    print('больше чем остаток')
        operations = {
            'phone_num': self.number,
            'date': datetime.now(),
            'account_from': 'debit_card',
            'account_to': 'savings_account',
            'amount': amount,
            'comment': input('comment transaction ')
        }
        transaction.append(operations)

    def transfer_money(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                if amount <= user['sav_account']:
                    user['sav_account'] -= amount
                    user['debit_card'] += amount
                else:
                    print('больше чем остаток')
        operations = {
            'phone_num': self.number,
            'date': datetime.now(),
            'account_from': 'savings_account',
            'account_to': 'debit_card',
            'amount': amount,
            'comment': input('comment transaction')
        }
        transaction.append(operations)


class DepositAccount(BankAccount):
    def add_deposit_90(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                if amount <= user['debit_card']:
                    user['debit_card'] -= amount
                    user_depo = {
                        'deposit_90': amount,
                        'deposit_90_date_cr': act_date
                    }
                    user.append(user_depo)
                    operations = {
                    'phone_num': self.number,
                    'date': datetime.now(),
                    'account_from': 'debit_card',
                    'account_to': 'deposit_90',
                    'amount': amount,
                    'comment': input('comment transaction ')
                    }
                    transaction.append(operations)

    def add_deposit_180(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                if amount <= user['debit_card']:
                    user['debit_card'] -= amount
                    user_pen = {
                        'deposit_180': amount,
                        'deposit_180_date_cr': act_date
                    }
                    user.append(user_pen)
                    
    def add_deposit_360(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                if amount <= user['debit_card']:
                    user['debit_card'] -= amount
                    user_pen = {
                        'deposit_360': amount,
                        'deposit_360_date_cr': act_date
                    }
                    user.append(user_pen)
                    operations = {
                        'phone_num': self.number,
                        'date': datetime.now(),
                        'account_from': 'debit_card',
                        'account_to': 'deposit_360',
                        'amount': amount,
                        'comment': input('comment transaction ')
                    }
                    transaction.append(operations)



class PensionAccount(BankAccount):
    
    def add_pen_acc():
        for user in data:
            if user['phone_num'] == self.number:
                user_pen = {
                                'pen_account': amount,
                                'pension_date_cr': act_date
                            }
                user.append(user_pen)
                operations = {
                    'phone_num': self.number,
                    'date': datetime.now(),
                    'account_from': 'debit_card',
                    'account_to': 'pen_account',
                    'amount': amount,
                    'comment': input('comment transaction ')
                    }
                transaction.append(operations)


                
    def add_money(self, amount):
        for user in data:
            if user['phone_num'] == self.number:
                if amount <= user['debit_card']:
                    user['debit_card'] -= amount
                    user['pen_account'] += amount
                else:
                    print('больше чем остаток')
                operations = {
                    'phone_num': self.number,
                    'date': datetime.now(),
                    'account_from': 'debit_card',
                    'account_to': 'pen_account',
                    'amount': amount,
                    'comment': input('comment transaction ')
                    }
                transaction.append(operations)


def show_transaction():
    for act in transaction:
        if act['phone_num'] == numb:
            if act['amount'] == int(input('какая была сумма транзакции: ')):
                print(act)


def bank_action_menu():
    while True:
        action = int(input('1 дебетовая карта, 2 накопительный счет, 3 deposite, 4 pension, 5 показать транзакции, 6 стоп: '))
        if action == 1:
            while True:
                action = int(input('1 внести , 2 снять, 3 стоп: '))
                if action == 1:
                    BankAccount(numb).add_money(int(input('Сколько внести: ')))
                elif action == 2:
                    BankAccount(numb).delete_money(int(input('Сколько снять: ')))
                elif action == 3:
                    save_users(data)
                    save_operations(transaction)
                    break
        elif action == 2:
            while True:
                action = int(input('1 внести с дебетового, 2 снять с дебетового, 3 стоп: '))
                if action == 1:
                    SavingsAccount(numb).add_money(int(input('Сколько внести: ')))
                elif action == 2:
                    SavingsAccount(numb).transfer_money(int(input('Сколько снять: ')))
                elif action == 3:
                    save_users(data)
                    save_operations(transaction)
                    break
        elif action == 3:
            while True:
                action = int(input('1 create 90 days deposit, 2 create 180 days deposit, 3 create 360 days deposit, 4 стоп: '))
                if action == 1:
                    DepositAccount(numb).add_deposit_90(int(input('Сколько внести: ')))
                elif action == 2:
                    DepositAccount(numb).add_deposit_180(int(input('Сколько внести: ')))
                elif action == 3:
                    DepositAccount(numb).add_deposit_360(int(input('Сколько внести: ')))
                elif action == 4:
                    save_users(data)
                    save_operations(transaction)
                    break
        elif action == 4:
            while True:
                action = int(input('1 добавить пенсионный счет, 2 внести с дебетового, 3 стоп: '))
                if action == 1:
                    PensionAccount(numb).add_pen_acc()
                elif action == 2:
                    PensionAccount(numb).add_money(int(input('Сколько : ')))
                elif action == 3:
                    save_users(data)
                    save_operations(transaction)
                    break
        elif action == 5:
            show_transaction()
        elif action == 6:
            break


# bank_action_menu()